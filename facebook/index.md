---
title: Facebook
subtitle: L'école des fans
lang: fr
date: 2018-10-22
year: 2018
month: 10
day: 22
rights: CC BY-SA 4.0
url: http://ateliers.sens-public.org/facebook
isbnprint: 978-2-924925-03-4
isbnepub: 978-2-924925-05-8
isbnpdf: 978-2-924925-04-1
authors:
  - forname: Gérard
    surname: Wormser
    orcidurl: https://orcid.org/0000-0002-6651-1650
backlinksp: Ce livre est une compilation d'essais écrits entre 2008 et 2018, publiés dans la revue *Sens public* et la revue la *Revue française de science politique* (vol. 64 / 3).
abstract_fr: >-
  *L’Ecole des fans* aborde les dimensions communicationnelles des
  disruptions contemporaines. Nos immenses possibilités d’accès aux
  échanges et aux savoirs contrastent avec les frustrations qui se
  développent au cœur des collectivités humaines – plus techniques et plus
  inégales que jamais. La connectivité universelle s’accompagne de
  ruptures brutales des liens d’intégration traditionnels. Elle abolit les
  médiations publiques au profit d’un mixte combinant des possibilités
  neuves à un isolement croissant. <br/>
  Soumis aux pressions qui nous emportent, comme l’indiquent leurs
  retombées aux USA ou au Brésil, les réseaux alimentent des régressions
  fantasmatiques et la peur de l’Autre. Au terme d’un parcours décrivant
  les manières dont nos réseaux associent la toute-puissance de l’argent à
  des chocs anthropologiques, Gérard Wormser rejoint les intuitions de
  Simmel qui avait perçu cette question voici plus d’un siècle. <br/>
  L’auteur suit au plus près la manière dont Mark Zuckerberg et ses
  proches surfent sur la vague qu’ils ont créée, car la croissance non
  maîtrisée de Facebook est un événement mondial aux effets managériaux et
  éditoriaux majeurs. Ses aspects touchent aussi bien notre vie
  quotidienne que la politique internationale, et les penser exige de
  passer par une enquête philosophique.
abstract_en:
description: Facebook - l'école des fans | Les ateliers de [sens public]
authorpresentation: Philosophe et éditeur formé à Paris, fondateur et directeur de _Sens public_ qu’il a conçu comme un réseau de ressources centré sur une revue, Gérard Wormser est spécialiste de phénoménologie morale et politique, auteur d’une thèse sur Jean-Paul Sartre. Après quinze ans à l’ENS de Lyon, il enseigne à l’Université de Rouen et est chercheur associé à l'Université de Brasília. Son action européenne et internationale s’est centrée sur la question des transformations culturelles et des réseaux de connaissance. Créateur du laboratoire _Éditorialisation des Sciences humaines et sociales_ (MSH Paris-Nord, 2007), organisateur des _Congrès Multilinguisme et travail en réseau_ (Eurozine 2008) et _La gouvernance du numérique éditorial_ (Cnrs-INHA, 2010), concepteur de nombreux séminaires en France et à l’étranger, sa réflexion sur l’anthropologie du numérique se déploie en de nombreuses publications. Il préside la Bourse Max Lazard à Sciences Po, contribue aux réseaux sartriens en Europe et au Brésil, où il a créé le blog ColetivoBrasil.
keyword_fr: Sartre, Facebook, Zuckerberg, liberté, éthique de l'information
keyword_en:
translator:
  - forname: ''
    surname: ''
toc:
pdfurl: media/Wormser_Facebook_2018.pdf
epuburl: media/Wormser_Facebook_2018.epub
luluurl: http://www.lulu.com/shop/g%C3%A9rard-wormser/facebook/paperback/product-23844860.html
amazonurl:
coverurl: img/couv_facebook.png
urlsp: http://sens-public.org/article1357.html?lang=fr
script: >-
  <script>
    $("#header").load("../header.html");
    $("#footer").load("../footer.html");
  </script>

---

test
