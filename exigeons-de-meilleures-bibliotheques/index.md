---
title: Exigeons de meilleures bibliothèques
subtitle: Plaidoyer pour une bibliothéconomie nouvelle
lang: fr
date: 2018-10-22
year: 2018
month: 10
day: 22
rights: CC BY-SA 4.0
url: http://ateliers.sens-public.org/exigeons-de-meilleures-bibliotheques
isbnprint: 978-2-924925-06-5
isbnepub: 978-2-924925-08-9
isbnpdf: 978-2-924925-07-2
authors:
  - forname: R. David
    surname: Lankes
    orcidurl:  https://orcid.org/0000-0001-6975-8045
backlinksp: Ce livre a fait l'objet d'une publication simultanée sur la revue *Sens public*.
abstract_fr: >-
  Les bibliothèques existent depuis des millénaires, mais sont-elles encore d’actualité aujourd'hui ? Dans un monde de plus en plus numérique et connecté, nos villes, nos collèges et nos écoles doivent-ils encore faire de la place aux livres ? Et si les bibliothèques ne se résument pas à leur collection de livres, quelle est donc leur fonction ? Dans son ouvrage, Lankes soutient que les communautés, pour prospérer, ont besoin de bibliothèques dont les préoccupations dépassent leurs bâtiments et les livres qu’ils contiennent. Nous devons donc attendre davantage de la part de nos bibliothèques. Elles doivent être des lieux d’apprentissage qui ont à cœur les intérêts de leurs communautés sur les enjeux de la protection de la vie privée, de la propriété intellectuelle et du développement économique. _Exigeons de meilleures bibliothèques_ est un cri de ralliement lancé aux communautés pour qu’elles haussent leurs attentes à l’égard des bibliothèques.
abstract_en:
description: Exigeons de meilleures bibliothèques | Les ateliers de [sens public]
authorpresentation: >- 	
  R. David Lankes est professeur et directeur de l'École de bibliothéconomie et des sciences de l’information de l'Université de South Carolina. Lankes a toujours tâché de mettre en relation la théorie et la pratique afin de mener des projets de recherche qui visent à changer les choses. Son travail a notamment été financé par la MacArthur Foundation, l'Institute for Library and Museum Services, la NASA, le département américain de l'Éducation, le département américain de la Défense, la National Science Foundation, le département d'État des États-Unis et l'American Library Association.

  Lankes est un ardent promoteur des bibliothèques et de leur rôle essentiel dans la société d'aujourd'hui. En 2016, il s'est vu décerner le prix Ken-Haycock de l'American Library Association afin de souligner son rôle dans la promotion de la bibliothéconomie auprès du grand public. Ses recherches portent également sur le rôle de l'information et des technologies dans la transformation de l'économie. À ce titre, il a pris part à des comités consultatifs et des équipes de recherche dans le domaine des bibliothèques, des télécommunications, de l'éducation et des transports, notamment au sein des National Academies of Sciences, Engineering, and Medicine (NASEM) des États-Unis. Il a été chercheur invité à Bibliothèque et Archives Canada et à la Harvard School of Education, en plus d'être le premier boursier de l'Office for Information Technology Policy de l'American Library Association. Son livre _The Atlas of New Librarianship_ s'est vu décerné, en 2012, le prix ABC-CLIO/Greenwood du meilleur ouvrage de bibliothéconomie.
keyword_fr: 'bibliothèque, bibliothéconomie, communauté, apprentissage, innovation, création de connaissances'
keyword_en: ''
translator:
  - forname: 'Jean-Michel'
    surname: 'Lapointe'
toc:
pdfurl: media/Lankes_Exigeons-de-meilleures-bibliothèques_2018.pdf
epuburl: media/Lankes_Exigeons-de-meilleures-bibliothèques_2018.epub
luluurl: http://www.lulu.com/shop/r-david-lankes/exigeons-de-meilleures-biblioth%C3%A8ques/paperback/product-23844888.html
amazonurl:
coverurl: img/couv_exigeons.png
urlsp: http://sens-public.org/article1358.html?lang=fr
script: >-
  <script>
    $("#header").load("../header.html");
    $("#footer").load("../footer.html");
  </script>

---

test
