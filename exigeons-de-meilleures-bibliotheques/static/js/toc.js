function generateHeadingsIds() {
  const content = document.querySelector('article')
  const headings = content.querySelectorAll(
    'h1, h2, summary, .dossier > h3, .video > h3, .pdf > h3'
  )
  const headingMap = {}

  Array.from(headings).forEach(heading => {
    const id = heading.id
      ? heading.id
      : heading.textContent
          .trim()
          .toLowerCase()
          .replace('\n', '')
          .split(' ')
          .filter(s => s)
          .join('-')
          .replace(/[\!\@\#\$\%\^\&\*\(\)\:“”«»\s]/gi, '')
    headingMap[id] = !isNaN(headingMap[id]) ? ++headingMap[id] : 0
    if (headingMap[id]) {
      heading.id = id + '-' + headingMap[id]
    } else {
      heading.id = id
    }
  })
}

function openReferencesOnClick() {
  const references = document.querySelector('.toc a[href="#références"]')
     if (!references) return
  references.addEventListener('click', e => {
    const anchor = e.target.getAttribute('href')
    const target = document.querySelector(anchor)
    target.parentElement.setAttribute('open', 'open')
    /* Waiting for tocboc to do its own scroll then scroll up a bit. Fragile. */
    setTimeout(() => {
      window.scrollTo({
        top: window.scrollY - 100,
        behavior: 'smooth'
      })
    }, 800)
  })
}

document.addEventListener('DOMContentLoaded', () => {
  generateHeadingsIds() // Useful to tocbot.
  tocbot.init({
    tocSelector: '.toc',
    contentSelector: 'article',
    headingSelector: 'h1, h2, summary, .dossier > h3, .video > h3, .pdf > h3',
    headingsOffset: 80
  })
  openReferencesOnClick()
})
