document.addEventListener('DOMContentLoaded', () => {
  const references = document.querySelector('#références')
  const chapter = document.body.dataset.chapitre
  if (!chapter) return
  Array.from(document.querySelectorAll(`[href^="#${chapter}-ref-"]`)).forEach(
    relativeLink => {
      /* Put attributes for balloon.css to render tooltips. */
      const reference = document.querySelector(relativeLink.hash)
      const content = reference.innerText
        .trim()
        .replace(/(?:https?|ftp):\/\/[\n\S]+/g, '')  // Remove links.
      const onelinerContent = content
        .split('\n')
        .map(fragment => fragment.trim())  // Remove new lines.
        .join(' ')
      relativeLink.setAttribute('aria-label', onelinerContent)
      relativeLink.dataset.balloonLength = 'xlarge'

      /* Open references on click. */
      relativeLink.addEventListener('click', e => {
        references.parentElement.setAttribute('open', 'open')
        // Waiting to reach the bottom of the page then scroll up a bit
        // to avoid the fixed header. Fragile.
        setTimeout(() => {
          window.scrollTo({
            top: window.scrollY - 100,
            behavior: 'smooth'
          })
        }, 10)
      })
    }
  )
})
