![Ateliers [sp]](textes/media/logo_ateliersSP.png)

Ce dépôt est celui du site de la maison d'édition _Les Ateliers de [sens public]_ et présente les sources des ouvrages publiés aux _Ateliers_.

Le site est accessible sur [ateliers.sens-public.org](https://ateliers.sens-public.org/).

## Conceptions

Au carrefour des pratiques de lecture classiques et des nouveaux usages liés au numérique, les ouvrages augmentés des _Ateliers de [sens public]_ proposent une complémentarité entre édition papier et édition numérique. Conçus par des chercheur·e·s en SHS, les ouvrages publiés par les _Ateliers_ se distinguent de la monographie académique traditionnelle en explorant des formes d’écriture alternatives&nbsp;: essai, manifeste, échanges épistolaires, carnet de recherche… L'objectif de la collection est de proposer des modèles d'écriture et de publication favorisant la conversation entre les chercheurs, la réappropriation du savoir et l'éditorialisation des contenus publiés. Un tel modèle suppose la mise au point d'une chaîne éditoriale innovante, basée sur des outils libres et ouverts, allant de _Pandoc_ à _GitLab_ en passant par _Zotero_ (gestion de références bibliographiques) et _Hypothes.is_ (annotation de documents web).

Les éditions augmentées s'appuient sur un système de publication modulaire et adaptable aux contenus, et qui engage une attention différente, disséminée et distribuée, privilégiant l’approfondissement par rebonds. Les _Ateliers_ entendent repenser le rôle de l'éditeur et, plus largement, la fonction éditoriale elle-même pour créer, au-delà de l'accès aux contenus, les conditions d'appropriation et d'interprétation des contenus.

Nos choix techniques et éditoriaux ont été établis selon cinq principes généraux&nbsp;:

- la granularité des contenus et la structuration fine des données
- la modularité de la chaîne éditoriale et des différents formats
- le low-tech appliqué aux formats et aux logiciels, comme garantie de soutenabilité et de pérennité de la chaîne et des contenus produits
- la pérennité des données et de leur accessibilité
- le logiciel libre, l'ouverture des sources et l'accès ouvert

## Boîte à outil

Les _Ateliers_ s'appuient sur les outils et formats suivants :

- les textes, les métadonnées et les références bibliographiques sont édités respectivement dans les formats _markdown_, _yaml_ et _bibtex_, à partir desquels sont produits des fichiers html statiques ;
- le script de production est écrit en _bash_, et mobilise les logiciels et langages suivants : _Pandoc_ (génération des contenus en html), _XSLT_ (enrichissement des htmls), _BaseX_ et _XQuery_ (production des index) ;
- les contenus sont édités sur un repo _Git_, hébergé par l'instance _Gitlab_ de _Framagit_ ;
- le corps de texte a été mis en page selon le style _Tufte_, avec la police de caractère _Jannon_.
