---
title: L'espace numérique
subtitle:
lang: fr
date: 2018-10-20
year: 2018
month: 10
day: 20
rights: CC BY-SA 4.0
url: http://ateliers.sens-public.org/l-espace-numerique
isbnprint: 978-2-924925-00-3
isbnepub: 978-2-924925-02-7
isbnpdf: 978-2-924925-01-0
authors:
  - forname: Éric
    surname: Méchoulan
    orcidurl: https://orcid.org/0000-0001-5659-5697
  - forname: Marcello
    surname: Vitali-Rosati
    orcidurl: https://orcid.org/0000-0001-6424-3229
backlinksp: Ce livre a fait l'objet d'une publication simultanée sous forme de feuilleton dans la revue *Sens public*.
abstract_fr: Comment penser l’espace numérique et rendre compte de son caractère à la fois structuré, mouvant et collectif ? Comment trouver un dispositif qui permet un dialogue ouvert, parvenant à saisir le sens des infrastructures numériques, sans les cristalliser en une essentialisation appauvrissante ? L'échange de courriels a semblé aux auteurs le moyen le plus approprié de faire de la théorie et de mettre en place un geste de pensée qui s’accorde avec la culture numérique, permettant d’envisager cette dernière avec un regard critique. Pendant un an et demi (de septembre 2015 à mars 2017), Éric Méchoulan et Marcello Vitali-Rosati ont donc échangé questions et réponses, afin d’essayer d’identifier les caractéristiques du numérique — espaces, temps et enjeux politiques — en continuité avec la tradition du dialogue philosophique.
abstract_en:
description: L'espace numérique | Les ateliers de [sens public]
authorpresentation: >-
  Professeur au Département des littératures de langue française depuis 1995, **Éric Méchoulan** a aussi été directeur de programme au Collège international de philosophie (2004-2010) et professeur invité dans de nombreuses institutions (Harvard, Paris 8, Paris 3, ENS Paris, ENS Lyon, ENS Haïti, Bloomington, Oran). Ses recherches actuelles portent sur les sentiments politiques à l’âge classique, sur l’histoire intermédiale des idées, sur les archives et la mémoire, ainsi que sur le temps qui passe. Il travaille aussi, dans un aller-retour entre philosophie et histoire, à mieux comprendre l’institution de l’esthétique et, en particulier, de ce que nous nommons «littérature». Il a fondé et dirigé jusqu’en 2006 la revue *Intermédialités*. Il est co-éditeur depuis 1996 de la revue américaine *Sub-Stance*. Il est responsable scientifique du Fonds Paul-Zumthor, directeur du Centre de Recherches Intermédiales sur les arts, les lettres et les techniques (CRIalt) de 2009 à 2013.

  **Marcello Vitali-Rosati** est professeur au département des littératures de langue française de l'Université de Montréal et titulaire de la Chaire de recherche du Canada sur les écritures numériques. Il développe une réflexion philosophique sur les enjeux des technologies numériques: le concept de virtuel, l'identité numérique, les notions d’auteur et d'autorité, les formes de production, légitimation et circulation du savoir à l'époque du web, et la théorie de l'éditorialisation - dont il est l'un des contributeurs les plus actifs. Il est l'auteur de nombreux articles et monographies et exerce également une activité d'éditeur en tant que directeur de la revue Sens public et co-directeur de la collection «&nbsp;Parcours Numériques&nbsp;» aux Presses de l'Université de Montréal.
keyword_fr: 'Espace numérique, Google, internet, philosophie, éditorialisation'
keyword_en:
translator:
  - forname: ''
    surname: ''
toc:
pdfurl: media/Mechoulan_Vitali-Rosati_L-espace-numerique_2018.pdf
epuburl: media/Mechoulan_Vitali-Rosati_L-espace-numerique_2018.epub
luluurl: http://www.lulu.com/shop/%C3%A9ric-m%C3%A9choulan-and-marcello-vitali-rosati/lespace-num%C3%A9rique/paperback/product-23844874.html
amazonurl:
coverurl: img/couv_espaceNumerique.png
urlsp: http://sens-public.org/article1319.html?lang=fr
script: >-
  <script>
    $("#header").load("../header.html");
    $("#footer").load("../footer.html");
  </script>

---

test
